//
//  HomeViewController.swift
//  News
//
//  Created by Rem Remy on 20/04/20.
//  Copyright © 2020 Rem Remy. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var HomeTableView: UITableView!
    
    private let categories = ["bisnis", "politik", "entertainment", "olahraga"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configCell()
        print("ini home")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("test")
    }
    
    func configCell(){
        HomeTableView.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "homeCell")
        HomeTableView.delegate = self
        HomeTableView.dataSource = self
    }
}

extension HomeViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell ", for: indexPath) as? HomeCell else {
          fatalError("Issue with dequeuing \("HomeCell")")
        }
        cell.categoryLb.text = categories[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let category = categories[indexPath.row]
        let viewController = NewsViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
}
