//
//  APIResponses.swift
//  News
//
//  Created by Rem Remy on 20/04/20.
//  Copyright © 2020 Rem Remy. All rights reserved.
//

import Foundation

struct NewsSummary: Codable{
    let count: Int?
    let results: [News]?
}

struct News: Codable{
    let source: Source
    let title: String
    let author: String?
    let desc: String
    let url: String
    let urlImg: String?
    let publishedDate: Date
    let content: String
}

struct Source: Codable{
    let id: Int?
    let name: String
}
