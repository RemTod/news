//
//  NetworkManager.swift
//  News
//
//  Created by Rem Remy on 20/04/20.
//  Copyright © 2020 Rem Remy. All rights reserved.
//

import Foundation

final class NetworkManager{
    var news: [String] = []
    private let domainUrlString = "http://newsapi.org/v2/top-headlines?" +
    "country=us&" +
    "apiKey=8f17ae7f0a5947019e8b5bc6b951f49f"
    
    func fetchNews(completionHandler: @escaping ([News]) -> Void) {
        let url = URL(string: domainUrlString)!
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("Error with fetching news: \(error)")
                return
            }

            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                print("Error with the response, unexpected status code: \(response)")
                return
            }

            if let data = data,
              let newsSummary = try? JSONDecoder().decode(NewsSummary.self, from: data) {
              completionHandler(newsSummary.results ?? [])
            }
        })
        task.resume()
    }
}
