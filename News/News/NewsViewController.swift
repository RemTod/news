//
//  NewsViewController.swift
//  News
//
//  Created by Rem Remy on 21/04/20.
//  Copyright © 2020 Rem Remy. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
    
    private var news: [News]?
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configCell()
        print("ini news")
        // Do any additional setup after loading the view.
    }
    
    func configCell(){
        tableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "newsCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension NewsViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsCell else {
          fatalError("Issue dequeuing \("NewsCell")")
        }
        
        cell.authorLb.text = news![indexPath.row].author
        cell.headlineLb.text = news![indexPath.row].title
        cell.contentLb.text = news![indexPath.row].desc
        cell.dateLb.text = String(describing: news![indexPath.row].publishedDate)
        cell.imageView?.image = UIImage(named: news![indexPath.row].urlImg!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
