//
//  NewsCell.swift
//  News
//
//  Created by Rem Remy on 21/04/20.
//  Copyright © 2020 Rem Remy. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var headlineLb: UILabel!
    @IBOutlet weak var authorLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var contentLb: UILabel!
    @IBOutlet weak var newImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
